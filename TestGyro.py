import sense_hat
import time
from sense_hat import SenseHat
compass_enabled = True
gyro_enabled = True
compass_enabled = False
accel_enabled = False

sense = sense_hat.SenseHat()
senseGyro = SenseHat()
sense.set_imu_config(False, True, False)
sense.low_light = True


def ContenuMess():
    FutureInput = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'\
                   ,'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '!', '+', '_', '?') #- = S  ! = V  ? = preview  _ = espace  + = R
    CurrChar = 0
    st = ""
            
    while True:
        sense.show_letter(FutureInput[CurrChar], back_colour = (0,0,0))
       
        events = sense.stick.get_events()
        if events:
            for event in events:
                if event.action !='pressed' :
                    continue
                if event.direction == "left":
                    CurrChar -= 1
                if event.direction == "right":
                    CurrChar +=1
                if event.direction == "down":
                    CurrChar -= 10
                if event.direction == "up":
                    CurrChar += 10
                
                if event.direction == "middle" :
                    if CurrChar == 36 and len(st) > 0:
                        st = st[:-1]
                        sense.show_letter(FutureInput[CurrChar], back_colour = (255,255,255))
                    if CurrChar == 37 and len(st) > 0:
                        return st
                    if CurrChar == 38:
                        Accueil()
                    if CurrChar == 39:
                        st += " "
                        sense.show_letter(FutureInput[CurrChar], back_colour = (255,255,255))
                    if CurrChar == 40:
                        sense.show_message(st, 0.055)
                    if CurrChar >= 0 and CurrChar <= 35:
                        st = st + str(FutureInput[CurrChar])
                        sense.show_letter(FutureInput[CurrChar], back_colour = (255,255,255))
                        time.sleep(0.5)
                
                if CurrChar > 40:
                    CurrChar = CurrChar - 41
                if CurrChar < 0:
                    CurrChar = 41 + CurrChar

def MessEtMdp():
    MessageSecret = ContenuMess()
    sense.show_letter('O', back_colour = (255,255,255))
    mdp = ""
    while len(mdp) < 6:
        events = sense.stick.get_events()
        if events:
            for event in events:
                if event.action !='pressed' :
                    continue
                if event.direction == "middle" :
                    sense.show_letter('O', back_colour = (255,255,255))
                    time.sleep(0.25)
                    sense.show_letter('', back_colour = (0,0,0))
                    raw = sense.get_accelerometer_raw()
                    s = "{x}|{y}|{z}".format(**raw)
                    s = s.split("|")
                    x = float(s[0])
                    y = float(s[1])
                    z = float(s[2])
                    if z > 0.75:
                        mdp += '0'
                    if y > 0.75:
                        mdp += '1'
                    if y < -0.75:
                        mdp += '2'
                    if x < -0.75:
                        mdp += '3'
                    if x > 0.75:
                        mdp += '4'
                    if z < -0.75:
                        mdp += '5'

    file = open("clef.txt","w")
    clef = hashing(mdp)
    file.write(clef)
    file.close()

    crypt=encode(mdp, MessageSecret)
    file = open("cryptmessage.txt","w")
    file.write(crypt)
    file.close()
    Accueil()


    
def Accueil():
    while True:

        sense.show_message("Lire mess. = haut  Supprimer mess. = gauche  Nouveau mess. = bas", 0.045)

        events = sense.stick.get_events()
        if events:
            for event in events:
                if event.action !='pressed' :
                    continue
                if event.direction == "down":
                    MessEtMdp()
                if event.direction == "up":
                    file = open("cryptmessage.txt","r")
                    Comparaison = file.read()
                    file.close()
                    if len(Comparaison) == 0:
                        sense.show_message("Aucun message", 0.1)
                        Accueil()
                    Confirmation = Confirmer()
                    if Confirmation != False:
                        BonMessage = decode(Confirmation, Comparaison)
                        for xy in range(3):
                            sense.show_message(BonMessage, 0.05)
                            time.sleep(0.5)
                        Accueil()
                if event.direction == "left":
                    file = open("clef.txt", "w")
                    file.write("")
                    file.close()
                    file = open("cryptmessage.txt", "w")
                    file.write("")
                    file.close()
                    sense.show_message("Message supprime",0.05)
                    Accueil()
                    

def encode(pwd, plain_text):
    key = pwd
    enc = []
    for i, e in enumerate(plain_text):
        key_c = key[i % len(key)]
        enc_c = chr((ord(e) + ord(key_c)) % 256)
        enc.append(enc_c)
    return ("".join(enc).encode()).decode()



def decode(pwd, cipher_text):
    key = pwd
    dec = []
    for i, e in enumerate(cipher_text):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(e) - ord(key_c)) % 256)
        dec.append(dec_c)
    return str("".join(dec))

def hashing(pwd):

    def to_32(value):
 
        value = value % (2 ** 32)
        if value >= 2**31:
            value = value - 2 ** 32
        value = int(value)
        return value

    if pwd:
        x = ord(pwd[0]) << 7
        m = 1000003
        for c in pwd:
            x = to_32((x*m) ^ ord(c))
        x ^= len(pwd)
        if x == -1:
            x = -2
        return str(x)
    return ""

def Confirmer():
    time.sleep(1)
    count = 0
    file = open("clef.txt", "r")
    s = file.read()
    file.close()
    sense.show_letter('o', back_colour = (255,255,255))
    while count < 3:
        Tentative = ""
        while len(Tentative) < 6:
            events = sense.stick.get_events()
            if events:
                for event in events:
                    if event.action != 'pressed':
                        continue
                    if event.direction == "middle":
                        sense.show_letter('o', back_colour = (255,255,255))
                        time.sleep(0.25)
                        sense.show_letter('', back_colour = (0,0,0))
                        raw = sense.get_accelerometer_raw()
                        pp = "{x}|{y}|{z}".format(**raw)
                        pp = pp.split("|")
                        x = float(pp[0])
                        y = float(pp[1])
                        z = float(pp[2])
                        if z > 0.75:
                            Tentative += '0'
                        if y > 0.75:
                            Tentative += '1'
                        if y < -0.75:
                            Tentative += '2'
                        if x < -0.75:
                            Tentative += '3'
                        if x > 0.75:
                            Tentative += '4'
                        if z < -0.75:
                            Tentative += '5'
        if len(Tentative) == 6:
            if hashing(Tentative) != s:
                count += 1
                sense.show_message("Nope", 0.05)
            if hashing(Tentative) == s:
                return Tentative

    sense.show_message("Oopsi", 0.1)
    file = open("clef.txt", "w")
    file.write("")
    file.close()
    file = open("cryptmessage.txt", "w")
    file.write("")
    file.close()
    return False
Accueil()