import sense_hat
import time
from sense_hat import SenseHat
compass_enabled = True
gyro_enabled = True
compass_enabled = False
accel_enabled = False

sense = sense_hat.SenseHat()
senseGyro = SenseHat()
sense.set_imu_config(False, True, False)
sense.low_light = True


def ContenuMess():
    FutureInput = ('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'\
                   ,'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', '!', '+', '_', '?') #- = S  ! = V  ? = preview  _ = espace  + = R
    CurrChar = 0
    st = ""    #CurrChar va servir d'index dans le tuple FutureInput. "st" est le string qui va contenir le message secret
            
    while True:
        sense.show_letter(FutureInput[CurrChar], back_colour = (0,0,0))
        #Cela affiche donc l'index CurrChar du tuple FutureInput à l'écran. CurrChar début à 0 donc au début nous voyons 'A'
        events = sense.stick.get_events()
        if events:
            for event in events:
                if event.action !='pressed' :
                    continue
                if event.direction == "left":
                    CurrChar -= 1    #On affiche le char précédant lorsque l'utilisateur fait gauche avec le joystick
                if event.direction == "right":
                    CurrChar +=1     #On affiche le char suivant lorsque l'utilisateur fait droite avec le joystick
                if event.direction == "down":
                    CurrChar -= 10   #On affiche le char 10 places plus loin pour une navigation plus  confortable lorsque l'utilisateur fait bas avec le joystick
                if event.direction == "up":
                    CurrChar += 10   #On revient 10 char en arrière pour une navigation plus confortable lorsque l'utilisateur fait haut avec le joystick
                
                if event.direction == "middle" : #quand on appuie sur le joystick, on regarde où on en est dans le tuple
                    if CurrChar == 36 and len(st) > 0: #Si on est sur '-' et que le message n'est pas vide:
                        st = st[:-1]                   #supprime le dernier char et fait un flash sur l'écran pou que l'utilisateur sache que ça a été validé
                        sense.show_letter(FutureInput[CurrChar], back_colour = (255,255,255))
                    if CurrChar == 37 and len(st) > 0: #Si on est sur '!', c'est qu'on a fini et donc la fonction va return st
                        return st
                    if CurrChar == 38:      #Si on est sur '+', on retourne au menu principal: on a fait retour en quelques sortes
                        Accueil()
                    if CurrChar == 39:      #Si on est sur '_', on fait un espace
                        st += " "
                        sense.show_letter(FutureInput[CurrChar], back_colour = (255,255,255))
                    if CurrChar == 40:      #Si on est sur '?', on a accès à une preview de st actuellement au cas où l'utilisateur sait plus où il en
                        sense.show_message(st, 0.055)    #est
                    if CurrChar >= 0 and CurrChar <= 35:   #Si le char actuel est soit un chiffre soit une lettre
                        st = st + str(FutureInput[CurrChar])  #Nous rajoutons ce char à la chaîne de caractère 'st'
                        sense.show_letter(FutureInput[CurrChar], back_colour = (255,255,255))
                        time.sleep(0.5)
                
                if CurrChar > 40:
                    CurrChar = CurrChar - 41 #Permet de revenir au début lorsqu'on dépasse la fin ('?' -> joystick droit -> 'A')
                if CurrChar < 0:
                    CurrChar = 41 + CurrChar #Permet d'aller à la fin lorsqu'on dépasse le début ('A' -> joystick gauche -> '?')

def MessEtMdp():
    MessageSecret = ContenuMess()  #on enregistre le st de ContenuMess() dans MessageSecret
    sense.show_letter('O', back_colour = (255,255,255)) #l'écran flash : là l'utilisateur va définir le mdp qui sera assigné à ce MessageSecret
    mdp = "" #mdp vide au début
    
    while len(mdp) < 6: #on a décidé que le mdp serait de 6 char. Ni plus ni moins. (plus de 6 on oubliait déjà facilement ce que c'était)
        events = sense.stick.get_events()
        if events:
            for event in events:
                if event.action !='pressed' :
                    continue
                if event.direction == "middle" : #Lorsque l'on appuie sur le joystick, l'écran flash et affiche un '?'
                    sense.show_letter('O', back_colour = (255,255,255))
                    time.sleep(0.25)
                    sense.show_letter('', back_colour = (0,0,0))
                    raw = sense.get_accelerometer_raw()
                    s = "{x}|{y}|{z}".format(**raw)
                    s = s.split("|")      #Le Raspberry va regarder
                    x = float(s[0])       #quelle est sa position
                    y = float(s[1])       #dans l'espace et c'est ce qui
                    z = float(s[2])       #va définir le mdp
                    if z > 0.75:
                        mdp += '0'
                    if y > 0.75:
                        mdp += '1'
                    if y < -0.75:
                        mdp += '2'
                    if x < -0.75:   #Chaque position est assignée à une valeur: lorsque l'utilisateur appuie sur le joystick, la valeur assignée
                        mdp += '3'  #à la position actuelle va être ajoutée au mdp
                    if x > 0.75:
                        mdp += '4'
                    if z < -0.75:
                        mdp += '5'

    file = open("clef.txt","w")
    clef = hashing(mdp)  #on hash ce mdp et on stock ça dans clef.txt
    file.write(clef)
    file.close()

    crypt=encode(mdp, MessageSecret)
    file = open("cryptmessage.txt","w")
    file.write(crypt)  #On crypte le message et on le met dans cryptmessage.txt
    file.close()
    
    Accueil() #on retourne à l'accueil


    
def Accueil():
    while True:
        sense.show_message("Lire mess. = haut  Supprimer mess. = gauche  Nouveau mess. = bas", 0.045) #Instruction pour l'utilisateur

        events = sense.stick.get_events()
        if events:
            for event in events:
                if event.action !='pressed' :
                    continue
                if event.direction == "down": #Appelle MessEtMdp() lorsque l'utilisateur veut faire un nouveau message
                    MessEtMdp()
                    
                if event.direction == "up": #Si on veut lire le message:
                    file = open("cryptmessage.txt","r")
                    Comparaison = file.read() #On met le contenu de cryptmessage.txt dans Comparaison
                    file.close()
                    if len(Comparaison) == 0: 
                        sense.show_message("Aucun message", 0.1) #Si Comparaison est vide, c'est qu'il n'y a rien dans cryptmessage.txt et qu'il n'y
                        Accueil()  #a donc rien à lire -> retour à l'accueil
                    Confirmation = Confirmer() #Confirmation va falloir soit 'False' soit un string comprenant le bon mdp
                    if Confirmation != False: #Si Confirmation n'est pas faux -> c'est qu'il a return le bon mdp
                        BonMessage = decode(Confirmation, Comparaison) #On stock le bon message décrypté
                        for xy in range(3): #on affiche trois fois le message au cas où
                            sense.show_message(BonMessage, 0.05)
                            time.sleep(0.5)
                        Accueil() #on retourne à l'accueil
                if event.direction == "left":
                    file = open("clef.txt", "w") #supprime la clef
                    file.write("")
                    file.close()
                    file = open("cryptmessage.txt", "w") #supprime le message
                    file.write("")
                    file.close()
                    sense.show_message("Message supprime",0.05)
                    Accueil() #retourne à l'accueil
                    

def encode(pwd, plain_text): #fonction qu'on nous a donné qui permet de crypter le message
    key = pwd
    enc = []
    for i, e in enumerate(plain_text):
        key_c = key[i % len(key)]
        enc_c = chr((ord(e) + ord(key_c)) % 256)
        enc.append(enc_c)
    return ("".join(enc).encode()).decode()



def decode(pwd, cipher_text): #fonction qu'on nous a donné qui permet de décrypter le message
    key = pwd
    dec = []
    for i, e in enumerate(cipher_text):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(e) - ord(key_c)) % 256)
        dec.append(dec_c)
    return str("".join(dec))

def hashing(pwd): #Fonction qu'on nous a donné qui hash le mdp

    def to_32(value):
 
        value = value % (2 ** 32)
        if value >= 2**31:
            value = value - 2 ** 32
        value = int(value)
        return value

    if pwd:
        x = ord(pwd[0]) << 7
        m = 1000003
        for c in pwd:
            x = to_32((x*m) ^ ord(c))
        x ^= len(pwd)
        if x == -1:
            x = -2
        return str(x)
    return ""

def Confirmer():
    time.sleep(1)
    count = 0
    file = open("clef.txt", "r")
    s = file.read() #On stock le hash du mdp dans s puisque dans l'absolu, on a perdu le mdp donc faudra comparer les hash
    file.close()
    sense.show_letter('o', back_colour = (255,255,255))
    while count < 3: #3 tentatives sinon c'est raté
        Tentative = "" #mdp que l'utilisateur est en train d'entrer
        while len(Tentative) < 6: #va faire input son mdp à l'utilisateur de la même manière que dans MessEtMdp()
            events = sense.stick.get_events()
            if events:
                for event in events:
                    if event.action != 'pressed':
                        continue
                    if event.direction == "middle":
                        sense.show_letter('o', back_colour = (255,255,255))
                        time.sleep(0.25)
                        sense.show_letter('', back_colour = (0,0,0))
                        raw = sense.get_accelerometer_raw()
                        pp = "{x}|{y}|{z}".format(**raw)
                        pp = pp.split("|")
                        x = float(pp[0])
                        y = float(pp[1])
                        z = float(pp[2])
                        if z > 0.75:
                            Tentative += '0'
                        if y > 0.75:
                            Tentative += '1'
                        if y < -0.75:
                            Tentative += '2'
                        if x < -0.75:
                            Tentative += '3'
                        if x > 0.75:
                            Tentative += '4'
                        if z < -0.75:
                            Tentative += '5'
        if len(Tentative) == 6:
            if hashing(Tentative) != s: #si le hash du vrai code est pas le même que ce hash-ci, c'est que le mdp est erroné
                count += 1  #une tentative de perdue 
                sense.show_message("Nope", 0.05) #nope
            if hashing(Tentative) == s: #Si les deux hash sont identiques, c'est que Tentative est le bon mdp. Dont même si on a perdu le mdp d'origine,
                return Tentative #on peut return Tentative qui est lui aussi le bon mdp en soit.

    sense.show_message("Oopsi", 0.1) #si on se trompe 3x de mdp -> message et clef supprimés par sécurité
    file = open("clef.txt", "w")
    file.write("")
    file.close()
    file = open("cryptmessage.txt", "w")
    file.write("")
    file.close()
    return False #Le programme renvoie False

Accueil() #pour commencer par Accueil()