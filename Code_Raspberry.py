import time
from sense_hat import SenseHat

def ContenuMess():
    FutureInput = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'S', 'V', 'R')
    CurrChar = 0
    st = ""
    s = SenseHat()  
            
    while True:
        s.show_letter(FutureInput[CurrChar], back_colour = (0,0,0))
       
        events = s.stick.get_events()
        if events:
            for event in events:
                if event.action !='pressed' :
                    continue
                if event.direction == "left" :
                    CurrChar -= 1
            
                if event.direction == "right" :
                    CurrChar +=1
                
                if event.direction == "up" :
                    if CurrChar == 10 and len(st) > 0:
                        st = st[:-1]
                        s.show_letter(FutureInput[CurrChar], back_colour = (255,255,255))
                    if CurrChar == 11 and len(st) > 0:
                        file = open("message.txt","w")
                        file.write(st)
                        file.close()
                        return st
                    if CurrChar >= 0 and CurrChar <= 9:
                        st = st + str(FutureInput[CurrChar])
                        s.show_letter(FutureInput[CurrChar], back_colour = (255,255,255))
                        time.sleep(0.5)
                
                if CurrChar > 12:
                    CurrChar = CurrChar - 13
                if CurrChar < 0:
                    CurrChar = 13 + CurrChar
                if event.direction == "down":
                    s.show_message(st)
 
ContenuMess()

